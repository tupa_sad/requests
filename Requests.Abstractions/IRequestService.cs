﻿using Requests.Abstractions.Models;
using Requests.Abstractions.Requests;
using Requests.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Requests.Abstractions
{
    public interface IRequestService
    {
        Task<IList<RequestResponse>> GetAllAsResponses(int page, int perPage);
        Task<Request> Get(string requestId);
        Task<Request> Create(Request request);
        Task Delete(string requestId);
        Task SetCompleted(string requestId, bool isComleted);
        Task<Request> Update(UpdateRequestRequest updateRequestRequest);
    }
}
