﻿using Requests.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Requests.Abstractions
{
    public interface IRequestRepository
    {
        Task<IEnumerable<Request>> GetAllRequests(int page, int perPage);
        Task<Request> GetRequest(string requestId);
        Task<Request> CreateRequestAsync(Request request);
        Task UpdateRequestAsync(Request request);
        Task DeleteRequestAsync(Request request);


        Task<IEnumerable<Patient>> GetAllPatients(int page, int perPage);
        Task<Patient> GetPatient(string patientId);
        Task<Patient> CreatePatientAsync(Patient patient);
        Task UpdatePatientAsync(Patient patient);
        Task DeletePatientAsync(Patient patient);


        Task<IEnumerable<Service>> GetAllServices(int page, int perPage);
        Task<Service> GetService(string serviceId);
        Task<Service> CreateServiceAsync(Service service);
        Task UpdateServiceAsync(Service service);
        Task DeleteServiceAsync(Service service);



    }
}
