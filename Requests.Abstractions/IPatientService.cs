﻿using Requests.Abstractions.Models;
using Requests.Abstractions.Requests;
using Requests.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Requests.Abstractions
{
    public interface IPatientService
    {
        Task<IList<PatientResponse>> GetAllAsResponses(int page, int perPage);
        Task<Patient> Get(string patientId);
        Task<Patient> Create(Patient patient);
        Task Delete(string patientId);
        Task<Patient> Update(UpdatePatientRequest updatePatientRequest);
    }
}
