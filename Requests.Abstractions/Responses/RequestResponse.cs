﻿using Requests.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Requests.Abstractions.Responses
{
    public class RequestResponse
    {
        public RequestResponse(Request request)
        {
            RequestId = request.RequestId;
            ServiceId = request.ServiceId;
            ClientId = request.ClientId;
            CreateAt = request.CreateAt;
            CloseAt = request.CloseAt;
            IsCompleted = request.IsCompleted;

            Employers = request.Employers.Select(e => e.EmployerId).ToList();
            Equipments = request.Equipments.Select(e => e.EquipmentId).ToList();
            Patients = request.Patients.Select(p => p.PatientId).ToList();
        }

        public string RequestId { get; set; }
        public string ServiceId { get; set; }
        public string ClientId { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime? CloseAt { get; set; }
        public bool IsCompleted { get; set; }

        public virtual ICollection<string> Employers { get; set; }
        public virtual ICollection<string> Equipments { get; set; }
        public virtual ICollection<string> Patients { get; set; }
    }
}
