﻿using Helpers;
using Requests.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Responses
{
    public class ServiceResponse : HaveNameDescription
    {
        public ServiceResponse(Service service)
        {
            ServiceId = service.ServiceId;
            Name = service.Name;
            Description = service.Description;
            Price = service.Price;
            IsAvaible = service.IsAvaible;
        }

        public string ServiceId { get; set; }
        public decimal Price { get; set; }
        public bool IsAvaible { get; set; }
    }
}
