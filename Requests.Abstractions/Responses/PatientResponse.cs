﻿using Helpers;
using Requests.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Responses
{
    public class PatientResponse : HaveNameDescription
    {
        public PatientResponse(Patient patient)
        {
            PatientId = patient.PatientId;
            Name = patient.Name;
            Description = patient.Description;
        }

        public string PatientId { get; set; }
    }
}
