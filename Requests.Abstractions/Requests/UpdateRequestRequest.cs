﻿using Employers.Abstractions.Models;
using Equipments.Abstractions.Models;
using Requests.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Requests
{
    public class UpdateRequestRequest
    {
        public string RequestId { get; set; }
        public string ServiceId { get; set; }
        public string ClientId { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime? CloseAt { get; set; }
        public bool IsCompleted { get; set; }

        public virtual ICollection<Employer> Employers { get; set; }
        public virtual ICollection<Equipment> Equipments { get; set; }
        public virtual ICollection<Patient> Patients { get; set; }

        public void UpdateRequest(ref Request request)
        {
            request.IsCompleted = IsCompleted;
        }
    }
}
