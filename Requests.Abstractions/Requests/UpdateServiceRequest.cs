﻿using Helpers;
using Requests.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Requests
{
    public class UpdateServiceRequest : HaveNameDescription
    {
        public string ServiceId { get; set; }
        public decimal Price { get; set; }
        public bool IsAvaible { get; set; }

        public void UpdateService(ref Service service)
        {
            service.Name = Name ?? service.Name;
            service.Description = Description ?? service.Description;
            service.Price = service.Price;
            service.IsAvaible = service.IsAvaible;
        }
    }
}
