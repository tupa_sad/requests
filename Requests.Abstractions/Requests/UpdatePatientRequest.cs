﻿using Helpers;
using Microsoft.Extensions.Primitives;
using Requests.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Requests
{
    public class UpdatePatientRequest : HaveNameDescription
    {
        public string PatientId { get; set; }

        public void UpdatePatient(ref Patient patient)
        {
            patient.Name = Name ?? patient.Name;
            patient.Description = Description ?? patient.Description;
        }
    }
}
