﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Requests
{
    public class CreateRequestRequest
    {
        public string ServiceId { get; set; }
        public string ClientId { get; set; }

        public virtual ICollection<string> Employers { get; set; }
        public virtual ICollection<string> Equipments { get; set; }
        public virtual ICollection<string> Patients { get; set; }
    }
}
