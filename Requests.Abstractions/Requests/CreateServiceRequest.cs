﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Requests
{
    public class CreateServiceRequest : HaveNameDescription
    {
        public decimal Price { get; set; }
        public bool IsAvaible { get; set; }
    }
}
