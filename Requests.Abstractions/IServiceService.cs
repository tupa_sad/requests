﻿using Requests.Abstractions.Models;
using Requests.Abstractions.Requests;
using Requests.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Requests.Abstractions
{
    public interface IServiceService
    {
        Task<IList<ServiceResponse>> GetAllAsResponses(int page, int perPage);
        Task<Service> Get(string serviceId);
        Task<Service> Create(Service service);
        Task Delete(string serviceId);
        Task SetAvaible(string serviceId, bool isAvaible);
        Task<Service> Update(UpdateServiceRequest updateServiceRequest);
    }
}
