﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Models
{
    public partial class Patient : HaveNameDescription
    {
        public string PatientId { get; set; }
    }
}
