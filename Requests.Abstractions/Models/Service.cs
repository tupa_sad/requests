﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Abstractions.Models
{
    public partial class Service : HaveNameDescription
    {
        public string ServiceId { get; set; }
        public decimal Price { get; set; }
        public bool IsAvaible { get; set; }

    }
}
