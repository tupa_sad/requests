﻿using Microsoft.EntityFrameworkCore;
using Requests.Abstractions;
using Requests.Abstractions.Models;
using Requests.Api.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Requests.Api.Services
{
    public class RequestRepository : IRequestRepository
    {
        private readonly RequestContext context;

        public RequestRepository(RequestContext context)
        {
            this.context = context;
        }

        public async Task<Patient> CreatePatientAsync(Patient patient)
        {
            context.Patients.Add(patient);
            await context.SaveChangesAsync();
            return patient;
        }

        public async Task<Request> CreateRequestAsync(Request request)
        {
            context.Requests.Add(request);
            await context.SaveChangesAsync();
            return request;
        }

        public async Task<Service> CreateServiceAsync(Service service)
        {
            context.Services.Add(service);
            await context.SaveChangesAsync();
            return service;
        }

        public async Task DeletePatientAsync(Patient patient)
        {
            await DeleteObjectFromDb(patient);
        }

        public async Task DeleteRequestAsync(Request request)
        {
            await DeleteObjectFromDb(request);
        }

        public async Task DeleteServiceAsync(Service service)
        {
            await DeleteObjectFromDb(service);
        }

        public async Task<IEnumerable<Patient>> GetAllPatients(int page, int perPage)
        {
            IQueryable<Patient> request = context.Patients;
            if (page > 0 && perPage > 0)
            {
                request = request.Skip((page - 1) * perPage).Take(perPage);
            }

            request = request.OrderBy(x => x.Name);
            return await request.ToListAsync();
        }

        public async Task<IEnumerable<Request>> GetAllRequests(int page, int perPage)
        {
            IQueryable<Request> request = context.Requests;
            if (page > 0 && perPage > 0)
            {
                request = request.Skip((page - 1) * perPage).Take(perPage);
            }

            return await request.ToListAsync();
        }

        public async Task<IEnumerable<Service>> GetAllServices(int page, int perPage)
        {
            IQueryable<Service> request = context.Services;
            if (page > 0 && perPage > 0)
            {
                request = request.Skip((page - 1) * perPage).Take(perPage);
            }

            request = request.OrderBy(x => x.Name);
            return await request.ToListAsync();
        }

        public async Task<Patient> GetPatient(string patientId)
        {
            Patient patient = context.Patients.FirstOrDefault(x => x.PatientId == patientId);
            if (patient == null)
            {
                throw new Exception($"Patient with '{patientId}' id is not found. ({nameof(RequestRepository)}.{nameof(GetPatient)})");
            }
            return patient;
        }

        public async Task<Request> GetRequest(string requestId)
        {
            Request request = context.Requests.FirstOrDefault(x => x.RequestId == requestId);
            if (request == null)
            {
                throw new Exception($"Request with '{requestId}' id is not found. ({nameof(RequestRepository)}.{nameof(GetRequest)})");
            }
            return request;
        }

        public async Task<Service> GetService(string serviceId)
        {
            Service service = context.Services.FirstOrDefault(x => x.ServiceId == serviceId);
            if (service == null)
            {
                throw new Exception($"Service with '{serviceId}' id is not found. ({nameof(RequestRepository)}.{nameof(GetService)})");
            }
            return service;
        }

        public async Task UpdatePatientAsync(Patient patient)
        {
            await UpdateObjectInDb(patient);
        }

        public async Task UpdateRequestAsync(Request request)
        {
            await UpdateObjectInDb(request);
        }

        public async Task UpdateServiceAsync(Service service)
        {
            await UpdateObjectInDb(service);
        }

        private async Task UpdateObjectInDb(object objectToUpdate)
        {
            context.Update(objectToUpdate);
            await context.SaveChangesAsync();
        }

        private async Task DeleteObjectFromDb(object objectToDelete)
        {
            context.Remove(objectToDelete);
            await context.SaveChangesAsync();
        }
    }
}
