﻿using Requests.Abstractions;
using Requests.Abstractions.Models;
using Requests.Abstractions.Requests;
using Requests.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Requests.Api.Services
{
    public class ServiceService : IServiceService
    {
        private readonly IRequestRepository requestRepository;

        public ServiceService(IRequestRepository requestRepository)
        {
            this.requestRepository = requestRepository;
        }

        public async Task<Service> Create(Service service)
        {
            return await requestRepository.CreateServiceAsync(service);
        }

        public async Task Delete(string serviceId)
        {
            Service service = await requestRepository.GetService(serviceId);
            if (service != null)
            {
                await requestRepository.DeleteServiceAsync(service);
            }
            return;
        }

        public async Task<Service> Get(string serviceId)
        {
            Service service = await requestRepository.GetService(serviceId);
            if (service == null)
            {
                return null;
            }
            return service;
        }

        public async Task<IList<ServiceResponse>> GetAllAsResponses(int page, int perPage)
        {
            var servicesDb = await requestRepository.GetAllServices(page, perPage);

            var services = new List<ServiceResponse>();
            foreach (var serviceDb in servicesDb)
            {
                services.Add(new ServiceResponse(serviceDb));
            }

            return services;
        }

        public async Task SetAvaible(string serviceId, bool isAvaible)
        {
            Service service = await Get(serviceId);
            service.IsAvaible = isAvaible;
            await requestRepository.UpdateServiceAsync(service);
        }

        public async Task<Service> Update(UpdateServiceRequest updateServiceRequest)
        {
            Service service = await requestRepository.GetService(updateServiceRequest.ServiceId);
            updateServiceRequest.UpdateService(ref service);
            await requestRepository.UpdateServiceAsync(service);
            return service;
        }
    }
}
