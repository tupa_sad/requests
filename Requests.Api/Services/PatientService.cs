﻿using Requests.Abstractions;
using Requests.Abstractions.Models;
using Requests.Abstractions.Requests;
using Requests.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Requests.Api.Services
{
    public class PatientService : IPatientService
    {
        private readonly IRequestRepository requestRepository;

        public PatientService(IRequestRepository requestRepository)
        {
            this.requestRepository = requestRepository;
        }

        public async Task<Patient> Create(Patient patient)
        {
            return await requestRepository.CreatePatientAsync(patient);
        }

        public async Task Delete(string patientId)
        {
            Patient patient = await requestRepository.GetPatient(patientId);
            if (patient != null)
            {
                await requestRepository.DeletePatientAsync(patient);
            }
            return;
        }

        public async Task<Patient> Get(string patientId)
        {
            Patient patient = await requestRepository.GetPatient(patientId);
            if (patient == null)
            {
                return null;
            }
            return patient;
        }

        public async Task<IList<PatientResponse>> GetAllAsResponses(int page, int perPage)
        {
            var patientsDb = await requestRepository.GetAllPatients(page, perPage);

            var patients = new List<PatientResponse>();
            foreach (var PatientDb in patientsDb)
            {
                patients.Add(new PatientResponse(PatientDb));
            }

            return patients;
        }

        public async Task<Patient> Update(UpdatePatientRequest updatePatientRequest)
        {
            Patient patient = await requestRepository.GetPatient(updatePatientRequest.PatientId);
            updatePatientRequest.UpdatePatient(ref patient);
            await requestRepository.UpdatePatientAsync(patient);
            return patient;
        }
    }
}
