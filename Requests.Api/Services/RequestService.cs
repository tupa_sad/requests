﻿using Requests.Abstractions;
using Requests.Abstractions.Models;
using Requests.Abstractions.Requests;
using Requests.Abstractions.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Requests.Api.Services
{
    public class RequestService : IRequestService
    {
        private readonly IRequestRepository requestRepository;

        public RequestService(IRequestRepository requestRepository)
        {
            this.requestRepository = requestRepository;
        }

        public async Task<Request> Create(Request request)
        {
            return await requestRepository.CreateRequestAsync(request);
        }

        public async Task Delete(string requestId)
        {
            Request request = await requestRepository.GetRequest(requestId);
            if (request != null)
            {
                await requestRepository.DeleteRequestAsync(request);
            }
            return;
        }

        public async Task<Request> Get(string requestId)
        {
            Request request = await requestRepository.GetRequest(requestId);
            if (request == null)
            {
                return null;
            }
            return request;
        }

        public async Task<IList<RequestResponse>> GetAllAsResponses(int page, int perPage)
        {
            var requestsDb = await requestRepository.GetAllRequests(page, perPage);

            var requests = new List<RequestResponse>();
            foreach (var requestDb in requestsDb)
            {
                requests.Add(new RequestResponse(requestDb));
            }

            return requests;
        }

        public async Task SetCompleted(string requestId, bool isComleted)
        {
            Request request = await Get(requestId);
            request.IsCompleted = isComleted;
            await requestRepository.UpdateRequestAsync(request);
        }

        public async Task<Request> Update(UpdateRequestRequest updateRequestRequest)
        {
            Request request = await requestRepository.GetRequest(updateRequestRequest.RequestId);
            updateRequestRequest.UpdateRequest(ref request);
            await requestRepository.UpdateRequestAsync(request);
            return request;
        }
    }
}
