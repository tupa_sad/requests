﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Requests.Abstractions;
using Requests.Api.Context;
using Requests.Api.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection InitializeRequestsServices(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> options = null)
        {
            services.AddDbContext<RequestContext>(options);
            services.TryAddScoped<IRequestRepository, RequestRepository>();
            services.TryAddScoped<IRequestService, RequestService>();
            services.TryAddScoped<IServiceService, ServiceService>();
            services.TryAddScoped<IPatientService, PatientService>();
            return services;
        }
    }
}
