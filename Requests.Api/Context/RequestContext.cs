﻿using Employers.Abstractions.Models;
using Microsoft.EntityFrameworkCore;
using Requests.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Requests.Api.Context
{
    public class RequestContext : DbContext
    {
        public RequestContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Request> Requests { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Patient> Patients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Patient>(entity =>
            {
                entity.ToTable("patient");

                entity.Property(e => e.PatientId)
                .HasColumnName("patient_id")
                .HasMaxLength(36);

                entity.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255);

                entity.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.ToTable("service");

                entity.Property(e => e.ServiceId)
                .HasColumnName("service_id")
                .HasMaxLength(36);

                entity.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255);

                entity.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(255);
            });

            modelBuilder.Entity<Request>(entity => 
            {
                entity.ToTable("request");

                entity.Property(e => e.RequestId)
                .HasColumnName("request_id")
                .HasMaxLength(36);

                entity.Property(e => e.ServiceId)
                .HasColumnName("service_id")
                .HasMaxLength(36);

                entity.Property(e => e.ClientId)
                .HasColumnName("client_id")
                .HasMaxLength(36);

                entity.Property(e => e.CreateAt)
                .HasColumnName("create_at")
                .HasDefaultValueSql("now()");

                entity.Property(e => e.CloseAt)
                .HasColumnName("close_at");

                entity.Property(e => e.IsCompleted)
                .HasColumnName("is_completed")
                .HasDefaultValue(false);

                entity.HasMany(e => e.Employers)
                .WithOne();

                entity.HasMany(e => e.Equipments)
                .WithOne();

                entity.HasMany(e => e.Patients)
                .WithOne();
            });
        }
    }
}
